const http = require("http");

const port = 3000;

const server = http.createServer((req, res) => {
    if(req.url == "/login") {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end("<h1>Welcome to the Login Page<h1>");
    } else {
        res.writeHead(404, {"Content-Type": "text/html"});
        res.end("<h1>I'm sorry the page you are looking for cannot be found.</h1>");
    }
});

server.listen(port);
console.log(`Server is now accessible at localhost:${port}`);
